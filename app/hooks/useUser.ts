import {useState, useEffect, useRef} from 'react';
import {decodeJwtToken} from "@/app/utils/jwtUtils";
import {getCookie} from "@/app/utils/cookieUtils";
import apiCall from "@/app/utils/apiCall";

const TOKEN_EXPIRATION_TIME_CHECK = 5*60*1000; // Every 5 minutes

let intervalRunning = false;
let intervalRef: NodeJS.Timeout|null = null;

export default function useUser() {
    const [user, setUser] = useState<User|null>(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const token = getCookie('token');

        if (!token) {
            setUser(null);
            setLoading(false);
            return;
        }

        const decoded = decodeJwtToken(token);

        if (!intervalRunning) {
            const checkTokenExpiration = (decoded: JwtToken) => {
                console.log("Checking token expiration")

                const expirationTime = (new Date(decoded.expiredAt)).getTime();

                if (expirationTime - Date.now() < TOKEN_EXPIRATION_TIME_CHECK) {
                    refreshToken(decoded.user);
                } else {
                    setUser(decoded.user); // User is still authenticated
                    setLoading(false);
                }
            };

            const refreshToken = async (user: User) => {
                console.log("refreshing token")

                // Make a request to the token refresh endpoint
                apiCall({path: '/auth/token-refresh', method: "POST"}).then(response => {
                    if (!response.ok && response.errors) {
                        console.error(response);
                        setUser(null);
                        setLoading(false);
                        return;
                    }

                    // localStorage.setItem('token', response.token)
                    setUser(user);
                    setLoading(false);
                });
            };

            // Initial check on component mount
            checkTokenExpiration(decoded);

            // Set up interval to check token expiration every 5 minutes
            intervalRef = setInterval(() => checkTokenExpiration(decoded), TOKEN_EXPIRATION_TIME_CHECK);
            intervalRunning = true;
        } else {
            setUser(decoded.user);
            setLoading(false);
        }

        // Clean up interval on component unmount
        return () => {
            if (intervalRef) {
                clearInterval(intervalRef);
                intervalRunning = false;
            }
        };
    }, []); // Empty dependency array means this effect runs once on mount

    return {user, loading};
}