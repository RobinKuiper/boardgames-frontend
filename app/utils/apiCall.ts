interface Params {
    path: string;
    method?: "POST"|"GET"|"PUT"|"PATCH"|"DELETE";
    body?: string;
}

export default async function apiCall({ path, method = "GET", body }: Params): Promise<any> {
    const options: RequestInit = {
        mode: 'cors',
        method: method ?? "GET",
        credentials: 'include', // Include cookies in the request
        headers: {
            'Content-Type': 'application/json'
        },
    };

    if (body) {
        options.body = body;
    }

    return fetch(`http://localhost:8000/api/v1${path}`, options)
        .then(response => {
            if (!response.ok) {
                return Promise.reject(response.json());
            }
            return response.json();
        })
        .then(data => {
            return data;
        })
        .catch(async error => {
            const err = await error;
            console.error('Request error:', err.message);
            return error;
        });
}