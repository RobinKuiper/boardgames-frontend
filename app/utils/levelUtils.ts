export function calculateExperienceDifferenceByLevel(level: number): number
{
    return Math.floor(1/4 * (level - 1 + 300 * 2 ** ((level - 1) / 7)));
}

export function calculateExperienceByLevel(level: number): number
{
    let experience = 0;

    for (let ell = 1; ell < level; ell++) {
        const innerSum = Math.floor(ell + 300 * 2 ** (ell / 7));
        experience += innerSum;
    }

    return Math.floor(experience / 4);
}

export function calculateLevelByExperience(targetExperience: number): number
{
    let experience = 0;
    let level = 1;

    while (experience <= targetExperience) {
        experience = calculateExperienceByLevel(level);
        level++;
    }

    return level - 2; // Subtracting 2 to get the level before the experience exceeds the target
}