"use client";

import React, {useEffect} from "react";
import useUser from "@/app/hooks/useUser";
import {redirect} from "next/navigation";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  const {user, loading} = useUser("Auth Layout");

  useEffect(() => {
    if (user && !loading) {
      // User is not authenticated, redirect to login page
      redirect('/game/woodcutting');
    }
  }, [user, loading]);

  if (loading) {
    return <p>Loading...</p>
  }

  return children;
}
