import "@/app/auth/styles/form.scss";
import "@/app/auth/register/components/styles/register-form.scss";
import Link from "next/link";

export default function RegisterForm() {
    return (
        <form>
            <label className="form-field">
                <span>Name</span>
                <input type="text" placeholder="Your name"/>
            </label>

            <label className="form-field">
                <span>Email</span>
                <input type="email" placeholder="Your email address"/>
            </label>

            <label className="form-field">
                <span>Password</span>
                <input type="password" placeholder="Your password"/>
            </label>

            <label className="form-field">
                <span>Password Confirmation</span>
                <input type="password" placeholder="Your password"/>
            </label>

            <div className="button-group">
                <button type="submit" className="register-button">Sign Up</button>
                <Link href="/auth/login" className="login-link">Sign In</Link>
            </div>
        </form>
    );
}