import '@/app/auth/register/styles/register.scss';
import RegisterForm from "@/app/auth/register/components/RegisterForm";

export default function Home() {
  return (
    <main className="registerpage">
        <div className="register-form">
            <h2>Sign Up</h2>

            <RegisterForm />
        </div>
    </main>
  )
}
