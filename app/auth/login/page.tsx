import '@/app/auth/login/styles/login.scss';
import LoginForm from "@/app/auth/login/components/LoginForm";

export default function Page() {
  return (
    <main className="loginpage">
        <div className="login-form">
            <h2>Sign In</h2>

            <LoginForm />
        </div>
    </main>
  )
}
