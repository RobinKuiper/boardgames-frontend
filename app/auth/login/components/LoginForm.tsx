"use client";

import "@/app/auth/styles/form.scss";
import "@/app/auth/login/components/styles/login-form.scss";
import Link from "next/link";
import {useState} from "react";
import apiCall from "@/app/utils/apiCall";
import {CustomResponse, Errors} from "@/app/types/Response";
import {redirect} from "next/navigation";

export default function LoginForm() {
    const [email, setEmail] = useState("robingjkuiper@gmail.com");
    const [password, setPassword] = useState("e}hF5w?A");
    const [errors, setErrors] = useState<Errors>({});

    const handleSubmit = async e => {
        e.preventDefault();

        const body = JSON.stringify({ email, password})

        const response: CustomResponse = await apiCall({ path: '/auth/login', method: "POST", body })

        if (!response.ok && response.errors) {
            setErrors(response.errors);
            return;
        }

        // redirect("/games/ours");
    }

    return (
        <form onSubmit={handleSubmit}>
            <label className="form-field">
                <span>Email</span>
                <input type="email" placeholder="Your email address" value={email} onChange={e => setEmail(e.target.value)} />
                {errors && errors.email && errors.email.map(error => <span key={error} className="error">{error}</span>)}
            </label>

            <label className="form-field">
                <span>Password</span>
                <input type="password" placeholder="Your password" value={password} onChange={e => setPassword(e.target.value)}/>
                {errors && errors.password && errors.password.map(error => <span key={error} className="error">{error}</span>)}
            </label>

            <div className="button-group">
                <button type="submit" className="login-button">Sign in</button>
                <Link href="/auth/register" className="register-link">Register</Link>
                <Link href="/game/woodcutting" className="register-link">woodcutting</Link>
            </div>
        </form>
    );
}