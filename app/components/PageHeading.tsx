import '@/app/components/styles/PageHeading.scss';
import React from "react";

interface Props {
    title: string;
    subtitle?: string;
    children?: React.ReactNode;
    right?: React.ReactNode;
}

export default function PageHeading({title, subtitle="", children="", right=""}: Props) {
    return (
        <div className='page-heading'>
            <div className="head">
                <div className="left">
                    <h1>{title}</h1>
                    <p>{subtitle}</p>
                </div>

                <div className="right">
                    {right}
                </div>
            </div>
            <div className="sub">
                {children}
            </div>
        </div>
    );
}