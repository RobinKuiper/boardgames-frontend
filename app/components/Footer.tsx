'use client';

import 'react-toastify/dist/ReactToastify.css';
import '@/app/components/styles/Footer.scss';

import Image from 'next/image';
import Link from 'next/link';
import React from 'react';
import { BiLogoGitlab } from 'react-icons/bi';
import { MdHomeRepairService } from 'react-icons/md';
import { ToastContainer } from 'react-toastify';
import { Tooltip } from 'react-tooltip';

import Logo from '@/images/logo.png';

const Footer = () => {
  return (
    <footer>
      <div className="logo">
        <Link className="image" href="https://www.rkuiper.nl" title="Rkuiper.nl" target="_blank">
          <Image src={Logo} alt="GemeenteOplossingen Logo" fill objectFit="contain" />
        </Link>
      </div>

      <nav>
        <Link href="https://gitlab.ontwikkeling.dev20" title="Gitlab" target="_blank">
          <BiLogoGitlab />
          Gitlab
        </Link>

        <Link href="https://servicedesk.gemeenteoplossingen.nl" title="Servicedesk" target="_blank">
          <MdHomeRepairService />
          ServiceDesk
        </Link>
      </nav>
      <div className="socials" />

      <ToastContainer theme="dark" />
      <Tooltip id="tooltip" />
    </footer>
  );
};

export default Footer;
