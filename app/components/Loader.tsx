'use client';

import '@/app/components/styles/Loader.scss';

import React from 'react';
import { RingLoader } from 'react-spinners';

interface Props {
    color?: string;
    text?: string;
    size?: number;
    fullHeight?: boolean;
}

export default function Loader({ color = '#96520d', text, size, fullHeight = false }: Props) {
    return (
        <div className={`loader-container ${fullHeight ? 'full-height-page' : ''}`}>
            <RingLoader color={color} size={size} />
            <span>{text}</span>
        </div>
    );
}
