"use client";

import '@/app/components/styles/Topbar.scss';

import Image from 'next/image';
import Link from 'next/link';
import React from 'react';

import Logo from '@/images/logo.webp';
import useUser from "@/app/hooks/useUser";

export default function TopBar() {
    const {user, loading} = useUser("Game Layout");

    return (
        <header>
            <div className="logo">
                <Link className="image" href="/" title="Remnant 2 Tools - Homepage">
                    <Image src={Logo} alt="GemeenteOplossingen Logo" fill objectFit="contain" />
                </Link>
            </div>

            <div className="right">
                <div>{user && `Account: ${user.name}`}</div>

            </div>
        </header>
    );
}
