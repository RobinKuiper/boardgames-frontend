"use client";

import "@/app/components/styles/Sidebar.scss";
import "@/app/components/styles/SidebarContent.scss";

import React, { useState } from 'react';
import { BiArrowToLeft, BiArrowToRight } from 'react-icons/bi';
import Link from "next/link";

interface Props {
    children?: React.ReactNode;
    side: 'left' | 'right';
}

const Sidebar = ({ children, side = 'left' }: Props) => {
    const [isOpen, setIsOpen] = useState<boolean>(false);
    const iconSize = '45px';
    const openIcon = side === 'left' ? <BiArrowToLeft size={iconSize} /> : <BiArrowToRight size={iconSize} />;
    const closeIcon = side === 'left' ? <BiArrowToRight size={iconSize} /> : <BiArrowToLeft size={iconSize} />;

    const toggleIsOpen = () => setIsOpen(!isOpen);

    return (
        <div className={`sidebar ${side} ${isOpen ? 'opened' : ''}`}>
            <div className="toggle" onClick={toggleIsOpen} onKeyDown={toggleIsOpen} role="toolbar">
                {isOpen ? openIcon : closeIcon}
            </div>

            <div className="sidebar-content">
                <div className="sidebar-content-container">
                    <button type="button" className="sidebar-button" title="Maak nieuwe aan">
                        Maak nieuwe aan
                    </button>

                    <nav>
                        <div>
                            <Link href='/games/all'>All Games</Link>
                        </div>
                        <div>
                            <Link href='/games/my'>My Games</Link>
                        </div>
                        <div>
                            <Link href='/games/ours'>Our Games</Link>
                        </div>
                    </nav>
                </div>
                {children}
            </div>
        </div>
    );
};

export default Sidebar;
