import '@/app/components/styles/IconLabel.scss';
import Image from "next/image";

interface Props {
    imgPath: string;
    text: string|number;
    label: string|number;
}

export default function IconLabel({ imgPath, text, label }: Props) {
    return (
        <span
            className='icon-label'
            data-tooltip-id="default-tooltip"
            data-tooltip-content={label.toString()}
            data-tooltip-place="top-start"
        >
            <Image
                src={imgPath}
                width={32}
                height={32}
                alt={label.toString()}
                title={label.toString()}
            />
            {text}
        </span>
    );
}