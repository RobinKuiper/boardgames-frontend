import {ActionGroup} from "@/app/enum/ActionGroup";
export default interface Action {
    id: number;
    name: string;
    description: string;
    baseTime: number;
    experience: number;
    actionGroup: ActionGroup;
}