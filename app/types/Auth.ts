interface JwtToken {
    user: User;
    expiredAt: string;
}

interface User {
    name: string;
    email: string;
    role: string;
    permissions: string[];
}