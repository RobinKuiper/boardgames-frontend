interface Character {
    id: number;
    name: string;
    gold: number;
    experience: number;
    skills: {
        woodcutting: number;
        mining: number;
    };
    inventory: InventoryItem[];
}

interface InventoryItem {
    id: number;
    item: Item;
    quantity: number;
}

interface Item {
    id: number;
    name: string;
    description: string;
    basePrice: number;
    rarity: string;
    equipmentType: string|null;
}