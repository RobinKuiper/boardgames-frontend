export interface Errors {
    [fieldName: string]: string[];
}

export interface CustomResponse extends Response {
    errors?: Errors;
    message?: string;
    status_code?: number;
    token?: string;
}