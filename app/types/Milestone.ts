interface Milestone {
    level: number,
    modifiers: Modifier[]
}

interface Modifier {
    group: "experience"|"drop",
    type: "amount"|"percentage",
    amount: number
}