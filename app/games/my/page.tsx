"use client";

import Link from "next/link";
import RemoteDataTable from "@/app/games/components/RemoteDataTable";
import useUser from "@/app/hooks/useUser";
import {CustomResponse} from "@/app/types/Response";
import apiCall from "@/app/utils/apiCall";
import DataTable from "@/app/games/components/DataTable";
import {useEffect, useState} from "react";
import useSWR from "swr";

export default function Page() {
    const { data: games, error, isLoading } =
        useSWR('/games/my', url => apiCall({ path: url }))

  return (
      <main className="action-page">
          {!isLoading && (
              <DataTable
                  title="My Games"
                  columns={[
                      {
                          name: 'Name',
                          selector: (game) => <Link href={`https://boardgamegeek.com/boardgame/${game.boardGameGeekId}`} target="_blank">{game.name}</Link>,
                          sortable: true,
                      },
                      { name: 'Year', sortable: true, selector: (game) => game.year },
                      { name: 'BGG ID', selector: (game) => game.boardGameGeekId, sortable: true },
                  ]}
                  data={games}
                  titleBg={false}
              />
          )}
      </main>
  )
}
