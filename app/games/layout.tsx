import '@/app/games/styles/layout.scss';

import React from 'react';

import Sidebar from '@/app/components/Sidebar';

interface Props {
    children: React.ReactNode;
}

export default async function Layout({ children }: Props) {
    return (
        <div className="category-layout-container">
            <Sidebar side="left"></Sidebar>

            <div className="category-layout-content">{children}</div>
        </div>
    );
}
