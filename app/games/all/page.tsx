"use client";

import Link from "next/link";
import RemoteDataTable from "@/app/games/components/RemoteDataTable";
import useUser from "@/app/hooks/useUser";
import {CustomResponse} from "@/app/types/Response";
import apiCall from "@/app/utils/apiCall";

export default function Page() {
    const { user, loading } = useUser();

    const addAction = async (row: { id: number }) => {
        const response: CustomResponse = await apiCall({
            path: `/games/${row.id}/add-user`,
            method: 'PATCH'
        })

        if (!response.ok && response.errors) {
            return;
        }
    };

  return (
      <main className="action-page">
          {!loading && (
              <RemoteDataTable
                  title="All Games"
                  columns={[
                      {
                          name: 'Name',
                          selector: (game) => <Link href={`https://boardgamegeek.com/boardgame/${game.boardGameGeekId}`} target="_blank">{game.name}</Link>,
                          sortkey: 'name',
                      },
                      { name: 'Year', sortkey: 'year', selector: (game) => game.year },
                      { name: 'BGG ID', selector: (game) => game.boardGameGeekId, sortkey: 'boardGameGeekId' },
                  ]}
                  url="/games"
                  addAction={user ? addAction : undefined}
                  titleBg={false}
              />
          )}
      </main>
  )
}
