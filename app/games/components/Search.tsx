import '@/app/games/components/styles/Search.scss';

import React, { useCallback, useEffect, useState } from 'react';
import { AiOutlineSearch } from 'react-icons/ai';
import { IoMdClose } from 'react-icons/io';

interface Props {
    callback: (result: string) => void;
    placeholder?: string;
}

export default function Search({ callback, placeholder = 'Search...' }: Props) {
    const [query, setQuery] = useState('');

    const cb = useCallback(callback, [callback]);

    useEffect(() => {
        cb(query);
    }, [query, cb]);

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setQuery(e.target.value);
    };

    const handleClear = () => {
        setQuery('');
    };

    return (
        <div className="search-container">
            <input type="text" placeholder={placeholder} value={query} onChange={handleChange} />
            <span>
        {query !== '' ? (
            <button onClick={handleClear} type="button">
                <IoMdClose />
            </button>
        ) : (
            <AiOutlineSearch />
        )}
      </span>
        </div>
    );
}
