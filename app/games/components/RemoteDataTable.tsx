'use client';

import '@/app/games/components/styles/DataTable.scss';

import React, { useEffect, useState } from 'react';
import {FaPlus, FaTrash} from 'react-icons/fa';
import { FaPencil } from 'react-icons/fa6';
import Pagination from '@/app/games/components/Pagination';
import apiCall from "@/app/utils/apiCall";
import DataTableLayout from "@/app/games/components/DataTableLayout";
import useSWR from "swr";

interface Props {
    title?: string;
    columns: any[];
    url: string;
    editAction?: (row: { id: number }) => void;
    addAction?: (row: { id: number }) => void;
    deleteAction?: (row: { id: number }) => void;
    highlightOnHover?: boolean;
    defaultSortFieldId?: string | null;
    titleBg?: boolean;
}

export default function RemoteDataTable({
                                      title,
                                      columns,
                                      url,
                                      editAction,
                                      deleteAction,
                                      addAction,
                                      highlightOnHover = true,
                                      defaultSortFieldId = null,
                                      titleBg = true,
                                  }: Props) {
    const cols = [...columns];
    if (editAction || deleteAction || addAction) {
        cols.push({
            name: '',
            cell: (row: { id: number }) => (
                <div className="actions-column">
                    {editAction && (
                        <button onClick={() => editAction(row)} type="button">
                            <FaPencil />
                        </button>
                    )}

                    {deleteAction && (
                        <button onClick={() => deleteAction(row)} type="button">
                            <FaTrash />
                        </button>
                    )}

                    {addAction && (
                        <button onClick={() => addAction(row)} type="button">
                            <FaPlus />
                        </button>
                    )}
                </div>
            ),
        });
    }

    const [tableData, setTableData] = useState([]);
    const [totalPages, setTotalPages] = useState(1);
    const [sortField, setSortField] = useState<string | null>(defaultSortFieldId ?? null);
    const [order, setOrder] = useState('asc');
    const [perPage, setPerPage] = useState(10);
    const [currentPage, setCurrentPage] = useState(1);
    const [query, setQuery] = useState('');
    const { data, error, isLoading } =
        useSWR(
            `${url}?start=${currentPage*perPage}&length=${perPage}&orderBy=${sortField}&orderDir=${order}&query=${encodeURIComponent(query)}`,
                url => apiCall({ path: url })
        )

    useEffect(() => {
        if (data && !isLoading && !error) {
            setTotalPages(data.recordsTotal / perPage)
            setTableData(data.data.map(row => cols.map(col => (col.selector ? col.selector(row) : col.cell(row)))))
        }
    }, [data]);

    const handleSortingChange = (accessor: number) => {
        const sortOrder = accessor === sortField && order === 'asc' ? 'desc' : 'asc';
        setSortField(accessor);
        setOrder(sortOrder);
    };

    const handleSearch = (query: string) => {
        setQuery(query);
    }

    return (
        <DataTableLayout
            title={title}
            titleBg={titleBg}
            highlightOnHover={highlightOnHover}
            columns={cols}
            data={tableData}
            handleSearch={handleSearch}
            handleSortingChange={handleSortingChange}
            order={order}
            sortField={sortField}
            loading={isLoading}
            pagination={(
                <Pagination
                    perPage={perPage}
                    setPerPage={setPerPage}
                    totalPages={totalPages}
                    setActivePage={setCurrentPage}
                    currentPage={currentPage}
                />
            )}
        />
    );
}
