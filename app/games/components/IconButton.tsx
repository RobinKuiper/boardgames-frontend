import '@/app/game/components/styles/IconButton.scss';
import Image from "next/image";

interface Props {
    title: string;
    imgPath: string;
    color?: string;
    disabledColor?: string;
    disabled?: boolean;
    onClick: () => void;
}

export default function IconButton({ title, imgPath, onClick, disabled=false, color="green", disabledColor="#035003" }: Props) {
    return (
        <button
            className="icon-button"
            onClick={onClick}
            disabled={disabled}
            style={{
                background: disabled ? disabledColor : color
            }}
            data-tooltip-id="default-tooltip"
            data-tooltip-content={title}
        >
            <Image
                src={imgPath}
                width={32}
                height={32}
                alt={title}
                title={title}
            />
        </button>
    )
}