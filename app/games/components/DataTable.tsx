'use client';

import '@/app/games/components/styles/DataTable.scss';

import {nanoid} from 'nanoid';
import React, {useEffect, useState} from 'react';
import {FaTrash} from 'react-icons/fa';
import {FaPencil} from 'react-icons/fa6';
import {TiArrowSortedDown, TiArrowSortedUp} from 'react-icons/ti';

import Pagination from '@/app/games/components/Pagination';
import DataTableLayout from "@/app/games/components/DataTableLayout";

interface Props {
    title?: string;
    columns: any[];
    data: any[];
    editAction?: (row: { id: number }) => void;
    deleteAction?: (row: { id: number }) => void;
    highlightOnHover?: boolean;
    defaultSortFieldId?: string | null;
    titleBg?: boolean;
}

export default function DataTable({
                                      title,
                                      columns,
                                      data = [],
                                      editAction,
                                      deleteAction,
                                      highlightOnHover = true,
                                      defaultSortFieldId = null,
                                      titleBg = true,
                                  }: Props) {
    const cols = [...columns];
    if (editAction || deleteAction) {
        cols.push({
            name: '',
            cell: (row: { id: number }) => (
                <div className="actions-column">
                    {editAction && (
                        <button onClick={() => editAction(row)} type="button">
                            <FaPencil/>
                        </button>
                    )}

                    {deleteAction && (
                        <button onClick={() => deleteAction(row)} type="button">
                            <FaTrash/>
                        </button>
                    )}
                </div>
            ),
        });
    }

    const initialData = data.map(row => cols.map(col => (col.selector ? col.selector(row) : col.cell(row))));
    const [tableData, setTableData] = useState(initialData.slice(0, 10));
    const [sortField, setSortField] = useState<string | null>(defaultSortFieldId ?? null);
    const [order, setOrder] = useState('asc');
    const [perPage, setPerPage] = useState(10);
    const [currentPage, setCurrentPage] = useState(1);
    const totalPages = initialData.length / perPage;

    const handleSorting = (accessor: string | null, sortOrder: string) => {
        if (accessor !== null) {
            const sorted = [...initialData].sort((a, b) => {
                if (a[accessor] === null) return 1;
                if (b[accessor] === null) return -1;
                if (a[accessor] === null && b[accessor] === null) return 0;
                return (
                    a[accessor].toString().localeCompare(b[accessor].toString(), 'en', {
                        numeric: true,
                    }) * (sortOrder === 'asc' ? 1 : -1)
                );
            });
            setTableData(sorted);
            return sorted;
        }
        return initialData;
    };

    const handleSortingChange = (accessor: string) => {
        const sortOrder = accessor === sortField && order === 'asc' ? 'desc' : 'asc';
        setSortField(accessor);
        setOrder(sortOrder);
    };

    const handlePageChange = (page: number, newData?: any[]) => {
        const nData = newData ?? tableData;
        const d = nData.slice((page - 1) * perPage, page * perPage);
        setTableData(d);
    };

    useEffect(() => {
        const sorted = handleSorting(sortField, order);
        handlePageChange(currentPage, sorted ?? undefined);
    }, [order, sortField, currentPage, perPage]);

    return (
        <DataTableLayout
            title={title}
            titleBg={titleBg}
            highlightOnHover={highlightOnHover}
            columns={cols}
            data={tableData}
            handleSortingChange={handleSortingChange}
            order={order}
            sortField={sortField}
            pagination={(
                <Pagination
                    perPage={perPage}
                    setPerPage={setPerPage}
                    totalPages={totalPages}
                    setActivePage={setCurrentPage}
                    currentPage={currentPage}
                />
            )}
        />
    );
}
