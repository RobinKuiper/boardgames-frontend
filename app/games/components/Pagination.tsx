import '@/app/games/components/styles/Pagination.scss';

interface Props {
    totalPages: number;
    currentPage: number;
    perPage: number;
    setActivePage: (page: number) => void;
    setPerPage: (items: number) => void;
}

export default function Pagination({ totalPages, setActivePage, currentPage, perPage, setPerPage }: Props) {
    const tPages = Math.ceil(totalPages);
    const pages = [];
    for (let i = 0; i < tPages; i += 1) {
        pages.push(i + 1);
    }

    return (
        <div className="pagination-container">
            <select value={perPage} onChange={e => setPerPage(Number(e.target.value))}>
                <option value={10}>10</option>
                <option value={25}>25</option>
                <option value={50}>50</option>
                <option value={100}>100</option>
                <option value={250}>250</option>
                <option value={500}>500</option>
            </select>

            <button type="button" disabled={currentPage === 1} onClick={() => setActivePage(currentPage - 1)}>
                {`<`}
            </button>

            <div className="numbers">
                {pages.length <= 10 && pages.map(option => (
                    <button
                        key={option}
                        type="button"
                        className={option === currentPage ? 'active' : ''}
                        disabled={currentPage === option}
                        onClick={() => setActivePage(option)}
                    >
                        {option}
                    </button>
                ))}

                {pages.length > 10 && (
                    <select value={currentPage} onChange={e => setActivePage(Number(e.target.value))}>
                        {pages.map(option => (
                            <option value={option}>{option}</option>
                        ))}
                    </select>
                )}
            </div>

            <button type="button" disabled={currentPage >= tPages} onClick={() => setActivePage(currentPage + 1)}>
                {`>`}
            </button>
        </div>
    );
}
