import Search from "@/app/games/components/Search";
import {nanoid} from "nanoid";
import {TiArrowSortedDown, TiArrowSortedUp} from "react-icons/ti";
import React from "react";
import Loader from "@/app/components/Loader";

interface Props {
    loading?: boolean;
    handleSearch?: (query: string) => void;
    handleSortingChange: (accessor: number) => void;
    order: string;
    sortField: string;
    data: any[];
    columns: any[];
    title?: string;
    highlightOnHover?: boolean;
    titleBg?: boolean;
    pagination?: React.ReactNode;
}

export default function DataTableLayout({
                                            loading = false,
                                            order,
                                            columns,
    sortField,
                                            handleSearch,
                                            data,
                                            handleSortingChange,
                                            title,
                                            highlightOnHover = true,
                                            titleBg = true,
                                            pagination = ''
                                        }: Props) {
    return (
        <div className="datatable-container">
            <div className="head">
                {title && (
                    <div id="title" className={`title ${titleBg ? 'bg' : ''}`}>
                        {title}
                    </div>
                )}

                <div className="loader">
                    {loading && <Loader />}
                </div>

                <div className="right">
                    {handleSearch && (
                        <div className="search">
                            <Search callback={handleSearch}/>
                        </div>
                    )}

                    {pagination}
                </div>
            </div>
            <table>
            <thead>
                <tr>
                    {columns.map(column => (
                        <th
                            key={nanoid()}
                            className={column.sortkey ? 'sortable' : ''}
                            onClick={() => (column.sortkey ? handleSortingChange(column.sortkey) : null)}
                        >
                            <div className="head">
                                <span>{column.name}</span>
                                {column.sortkey && (
                                    <span className={`sort-icon ${column.sortkey === sortField ? 'active' : ''}`}>
                      {order === 'asc' ? <TiArrowSortedUp/> : <TiArrowSortedDown/>}
                    </span>
                                )}
                            </div>
                        </th>
                    ))}
                </tr>
                </thead>

                {data.length || loading ? (
                    <tbody>
                    {data.map(row => (
                        <tr key={nanoid()} className={highlightOnHover ? 'highlightable' : ''}>
                            {row.map(column => (
                                <td key={nanoid()}>{column}</td>
                            ))}
                        </tr>
                    ))}
                    </tbody>
                ) : (
                    <tbody>
                    <tr>
                        <td colSpan={columns.length}>
                            {loading ? 'Loading...' : 'Geen data gevonden.'}
                        </td>
                    </tr>
                    </tbody>
                )}
            </table>

            {pagination}
        </div>
    )
}