"use client";

import Link from "next/link";
import apiCall from "@/app/utils/apiCall";
import DataTable from "@/app/games/components/DataTable";
import useSWR from "swr";

export default function Page() {
    const { data: games, error, isLoading } =
        useSWR('/games/ours', url => apiCall({ path: url }))

  return (
      <main className="action-page">
          {!isLoading && (
              <DataTable
                  title="Our Games"
                  columns={[
                      {
                          name: 'Name',
                          selector: (game) => <Link href={`https://boardgamegeek.com/boardgame/${game.boardGameGeekId}`} target="_blank">{game.name}</Link>,
                          sortable: true,
                      },
                      { name: 'Year', sortable: true, selector: (game) => game.year },
                      { name: 'BGG ID', selector: (game) => game.boardGameGeekId, sortable: true },
                      { name: 'User', selector: (game) => game.user, sortable: true },
                  ]}
                  data={games}
                  titleBg={false}
              />
          )}
      </main>
  )
}
