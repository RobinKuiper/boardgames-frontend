import {create} from "zustand";
import {devtools, persist} from "zustand/middleware";

interface CharacterState {
    character: Character|null;
    setCharacter: (character: Character) => void;
}

const useCharacterStore = create<CharacterState>()(
    devtools(
        persist(
            set => ({
                character: null,
                setCharacter: (character: Character) => set(() => ({ character }))
            }),
            {
                name: 'character-storage'
            }
        )
    )
)

export default useCharacterStore;