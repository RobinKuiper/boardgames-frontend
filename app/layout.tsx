import '@/app/styles/globals.scss';

import type { Metadata } from 'next';
import { Montserrat } from 'next/font/google';
import React from 'react';

import Footer from '@/app/components/Footer';
import TopBar from '@/app/components/Topbar';

const montserrat = Montserrat({ subsets: ['latin'] });

export const metadata: Metadata = {
    title: 'GO. Release Manager',
    description: '',
};

interface Props {
    children: React.ReactNode;
}

export default async function RootLayout({ children }: Props) {
    return (
        <html lang="en">
        <body className={montserrat.className}>
        <TopBar />

        <div className="content">{children}</div>

        <Footer />
        </body>
        </html>
    );
}